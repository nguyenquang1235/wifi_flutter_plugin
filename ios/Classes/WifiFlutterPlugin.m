#import "WifiFlutterPlugin.h"
#if __has_include(<wifi_flutter_plugin/wifi_flutter_plugin-Swift.h>)
#import <wifi_flutter_plugin/wifi_flutter_plugin-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "wifi_flutter_plugin-Swift.h"
#endif

@implementation WifiFlutterPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftWifiFlutterPlugin registerWithRegistrar:registrar];
}
@end
